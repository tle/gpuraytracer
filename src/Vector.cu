#include "Vector.h"

/*
 * This file contains definitions for functions declared in Vector.h
 */

// Vector equality operator
HEMI_DEV_CALLABLE_MEMBER
bool Vector::operator==(const Vector &v) const {
    return (x == v.x) && (y == v.y) && (z == v.z);
}

// Vector inequality operator
HEMI_DEV_CALLABLE_MEMBER
bool Vector::operator!=(const Vector &v) const {
    return !(*this == v);
}

// Vector self-addition with a scalar
HEMI_DEV_CALLABLE_MEMBER
Vector & Vector::operator+=(float number) {
    x += number;
    y += number;
    z += number;
    return *this;
}

// Vector self-subtraction with a scalar
HEMI_DEV_CALLABLE_MEMBER
Vector & Vector::operator-=(float number) {
    *this += (-number);
    return *this;
}

// Vector self-multiplication with a scalar
HEMI_DEV_CALLABLE_MEMBER
Vector & Vector::operator*=(float number) {
    x *= number;
    y *= number;
    z *= number;
    return *this;
}

// Vector self-division by a scalar
HEMI_DEV_CALLABLE_MEMBER
Vector & Vector::operator/=(float number) {
    x /= number;
    y /= number;
    z /= number;
    return *this;
}

// Vector-scalar addition
HEMI_DEV_CALLABLE
const Vector operator+(const Vector &v, float s) {
    Vector result(v);
    result += s;
    return result;
}

// Scalar-vector addition
HEMI_DEV_CALLABLE
const Vector operator+(float s, const Vector &v) {
    return v + s;
}

// Vector-scalar subtraction
HEMI_DEV_CALLABLE
const Vector operator-(const Vector &v, float s) {
    Vector result(v);
    result -= s;
    return result;
}

// Scalar-vector subtraction
HEMI_DEV_CALLABLE
const Vector operator-(float s, const Vector &v) {
    Vector result(v);
    result -= s;
    result *= -1.0f;
    return result;
}

// Vector-scalar multiplication (right multiplication)
HEMI_DEV_CALLABLE
const Vector operator*(const Vector &v, float s) {
    Vector result(v);
    result *= s;
    return result;
}

// Scalar-vector multiplication (left multiplication)
HEMI_DEV_CALLABLE
const Vector operator*(float s, const Vector &v) {
    return v * s;
}

// Vector-scalar division (right division)
HEMI_DEV_CALLABLE
const Vector operator/(const Vector &v, float s) {
    Vector result(v);
    result /= s;
    return result;
}

// Vector assignment
HEMI_DEV_CALLABLE_MEMBER
Vector & Vector::operator=(const Vector &v) {
    if (this != &v) {
        x = v.x;
        y = v.y;
        z = v.z;
    }
    return *this;
}

// Vector self-addition with a vector
HEMI_DEV_CALLABLE_MEMBER
Vector & Vector::operator+=(const Vector &v) {
    if (this != &v) {
        x += v.x;
        y += v.y;
        z += v.z;
    }
    return *this;
}

// Vector self-subtraction with a vector
HEMI_DEV_CALLABLE_MEMBER
Vector & Vector::operator-=(const Vector &v) {
    if (this != &v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }
    return *this;
}

// Vector norm
HEMI_DEV_CALLABLE_MEMBER
float Vector::norm() const{
    return sqrt(x * x + y * y + z * z);
}

// Vector-vector addition
HEMI_DEV_CALLABLE
const Vector operator+(const Vector &v, const Vector &s) {
    Vector result(v);
    result += s;
    return result;
}

// Vector-vector subtraction
HEMI_DEV_CALLABLE
const Vector operator-(const Vector &v, const Vector &u) {
    Vector result(v);
    result -= u;
    return result;
}

// Dot product
HEMI_DEV_CALLABLE
float dot(const Vector &u, const Vector &v){
    return u.x * v.x + u.y * v.y + u.z * v.z;
}

// Cross product
HEMI_DEV_CALLABLE
const Vector cross(const Vector &u, const Vector &v){
    Vector result;
    result.x = u.y * v.z - u.z * v.y;
    result.y = -(u.x * v.z - u.z * v.x);
    result.z = u.x * v.y - u.y * v.x;
    return result;
}

// Unit vector in the direction of the vector
HEMI_DEV_CALLABLE_MEMBER
Vector Vector::unit() const{
    Vector result(*this);
    result /= norm();
    return result;
}

// Vector projection of u onto v
HEMI_DEV_CALLABLE
const Vector project(const Vector &u, const Vector &v) {
    return (dot(u, v) / dot(v, v)) * v;
}

HEMI_DEV_CALLABLE
const Vector operator-(const Vector &v) {
    return -1.0 * v;
}

// Utility check whether a vector is a unit vector
HEMI_DEV_CALLABLE_MEMBER
bool Vector::isUnit() const {
    return norm() == 1.0f;
}

// Printing function

std::ostream& operator<<(std::ostream &os, const Vector &v) {
    os << "[" << v.x << " " << v.y << " " << v.z << "]" << std::endl;
    return os;
}


