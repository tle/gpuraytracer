#include "Ray.h"
#include <cfloat>

HEMI_DEV_CALLABLE_MEMBER
Ray::Ray(const Vector &orig, const Vector &dir, bool normalize) {
    origin = orig;
    if (normalize)
        direction = dir.unit();
    else
        direction = dir;

    tMin = 0;
    tMax = FLT_MAX;

    invDirection.x = 1.0f / direction.x;
    invDirection.y = 1.0f / direction.y;
    invDirection.z = 1.0f / direction.z;

    sign[0] = invDirection.x < 0;
    sign[1] = invDirection.y < 0;
    sign[2] = invDirection.z < 0;
}

Ray::Ray(const Ray &r) {
    origin = r.origin;
    direction = r.direction;
    tMin = r.tMin;
    tMax = r.tMax;
    invDirection = r.invDirection;
    sign[0] = invDirection.x < 0;
    sign[1] = invDirection.y < 0;
    sign[2] = invDirection.z < 0;

}

HEMI_DEV_CALLABLE_MEMBER
const Vector Ray::getPoint(float t) const {
    return origin + t * direction;
}

HEMI_DEV_CALLABLE_MEMBER
const Ray Ray::reflect(const Vector &location, Vector &normal) const{
    Vector Dpar = project(-direction, normal);
    Vector new_dir = direction + 2.0f * Dpar;
    return Ray(location + new_dir * REFLECTION_DELTA, new_dir, true);
}
