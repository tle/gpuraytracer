#include "Color.h"
#define EPSILON 1.19209E-07

HEMI_DEV_CALLABLE_MEMBER
bool Color::valid(float value) const {
    return (value >= 0) && (value <= 1);
}

HEMI_DEV_CALLABLE_MEMBER
float Color::clip(float value) const {
    if (value < 0)
        return 0;
    else if (value > 1)
        return 1;
    else
        return value;
}

HEMI_DEV_CALLABLE_MEMBER
Color::Color(float red, float green, float blue) {
    r = red;
    g = green;
    b = blue;
}

HEMI_DEV_CALLABLE_MEMBER
Color::Color(const Color &c) {
    r = c.getRed();
    g = c.getGreen();
    b = c.getBlue();
}
    
HEMI_DEV_CALLABLE_MEMBER
float Color::getRed() const {
    return r;
}

HEMI_DEV_CALLABLE_MEMBER
float Color::getGreen() const {
    return g;
}

HEMI_DEV_CALLABLE_MEMBER
float Color::getBlue() const {
    return b;
}

HEMI_DEV_CALLABLE_MEMBER
void Color::setRed(float value) {
    r = value;
}

HEMI_DEV_CALLABLE_MEMBER
void Color::setGreen(float value) {
    g = value;
}

HEMI_DEV_CALLABLE_MEMBER
void Color::setBlue(float value) {
    b = value;
}

HEMI_DEV_CALLABLE_MEMBER
bool Color::operator==(const Color &c) const {
    return fabs(r - c.getRed()) < EPSILON &&
           fabs(g - c.getGreen()) < EPSILON &&
           fabs(b - c.getBlue()) < EPSILON;
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator=(const Color &c) {
    if (this != &c) {
        r = c.getRed();
        b = c.getBlue();
        g = c.getGreen();
    }
    return *this;
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator+=(const Color &c) {
    if (this != &c) {
        r = clip(r + c.getRed());
        b = clip(b + c.getBlue());
        g = clip(g + c.getGreen());
    }
    return *this;
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator*=(const Color &c) {
    if (this != &c) {
        r = clip(r * c.getRed());
        b = clip(b * c.getBlue());
        g = clip(g * c.getGreen());
    }
    return *this;
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator-=(const Color &c) {
    if (this != &c) {
        r = clip(r - c.getRed());
        b = clip(b - c.getBlue());
        g = clip(g - c.getGreen());
    }
    return *this;
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator+=(float s) {
    r = clip(r + s);
    b = clip(b + s);
    g = clip(g + s);
    return *this;    
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator-=(float s) {
    r = clip(r - s);
    b = clip(b - s);
    g = clip(g - s);
    return *this;    
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator*=(float s) {
    r = clip(r * s);
    b = clip(b * s);
    g = clip(g * s);
    return *this;    
}

HEMI_DEV_CALLABLE_MEMBER
Color & Color::operator/=(float s) {
    r = clip(r / s);
    b = clip(b / s);
    g = clip(g / s);
    
    return *this;    
}

HEMI_DEV_CALLABLE_MEMBER
const Color Color::operator+(const Color &c) const {
    Color result(*this);
    result += c;
    return result;
}

HEMI_DEV_CALLABLE_MEMBER
const Color Color::operator-(const Color &c) const {
    Color result(*this);
    result -= c;
    return result;
}

HEMI_DEV_CALLABLE_MEMBER
const Color Color::operator*(const Color &c) const {
    Color result(*this);
    result *= c;
    return result;
}
    
std::ostream & operator<<(std::ostream &os, const Color &c) {
    os << "(" << c.getRed() << ", " << c.getGreen() << ", " << c.getBlue() << ")" << std::endl;
    return os;
}

HEMI_DEV_CALLABLE
const Color operator+(const Color &c, float s) {
    Color result(c);
    result += s;
    return result;
}

HEMI_DEV_CALLABLE
const Color operator+(float s, const Color &c) {
    return c + s;
}

HEMI_DEV_CALLABLE
const Color operator*(const Color &c, float s) {
    Color result(c);
    result *= s;
    return result;
}

HEMI_DEV_CALLABLE
const Color operator*(float s, const Color &c) {
    return c * s;
}

HEMI_DEV_CALLABLE
const Color operator/(const Color &c, float s) {
    Color result(c);
    result /= s;
    return result;
}

