#include "Scene.h"
using namespace std;

surface<void, 2> outputSurface;

Scene::~Scene() {
    for_each(objects.begin(), objects.end(), DeleteObject());
    for_each(lights.begin(), lights.end(), DeleteObject());
}

void Scene::addObject(SceneObject *obj) {
    objects.push_back(obj);
}

void Scene::addLight(Light *lt) {
    lights.push_back(lt);
}

HEMI_DEV_CALLABLE_MEMBER
SceneObject * Scene::findClosestObject(const Ray &r, float &tintersect) const{
    SceneObject *result = 0;
    tintersect = INF;
    int i = 0;
    for (i = 0; i < nObjects; i++) {
        float t = objectPointer[i]->intersection(r);
        if (t < tintersect && t != NO_INTERSECTION) {
            result = objectPointer[i];
            tintersect = t;
        }
    }
    return result;
}


HEMI_DEV_CALLABLE_MEMBER
const Color Scene::raytrace(const Ray &r, int depth) const {
    Color result(0, 0, 0);
    float factor  = 1.0f;
    int current_depth = depth;
    Ray currentRay = r;
    while (current_depth < MAX_DEPTH) {

        float t;
        SceneObject *closest = findClosestObject(currentRay, t);

        // no intersection found if pointer obtained is null
        if (closest == 0) {
            result += factor * getBackgroundColor(currentRay);
            break;
        }
        else {
            depth = 0;
            
            Vector intersectLoc = currentRay.getPoint(t);
            Vector N = closest->getNormalAtPoint(intersectLoc);
            Color objectColor = closest->getColorAtPoint(intersectLoc);
            // ambient lighting
            Color ambient = objectColor;
            Color diffuse, specular;
            // object color itself
            for (int i = 0; i < nLights; i++) {
                Light *li = lightPointer[i];
                Vector lightLoc = li->getPosition();
                
                // vector from light position to intersection location
                Vector L = (lightLoc - intersectLoc).unit();
                
                Color lightColor = li->getColor();
                
                float NdotL = max(dot(N, L), 0.0f);
                if (NdotL < 0)
                    continue;
                diffuse += lightColor * NdotL;
                
                // specular
                float k = dot(N, (L + (currentRay.origin-intersectLoc).unit()).unit());
                if (k > 0)  // for now arbitrary specular component
                    specular += pow((double) k, 0.3) * lightColor; 
                }
            // 'filter' the colors through each other
            result += factor * ambient * diffuse + 0.2 * specular;
            // simulate recursion
            if (closest->getReflectivity() != 0.0f) {
                currentRay = Ray(currentRay.reflect(intersectLoc, N));
                factor *= closest->getReflectivity();
                current_depth++;
            }
            else{
                break;
            }
        }
    }
    return result;
}

void Scene::render(const Camera &camera, int imageSize, int animation) const {
    // initialize data
    std::vector<u8> pixels;
    // declare a copy for the camera
    Camera cam = camera;

    clock_t startTime = clock();
    cout << "Render time for one frame (with texture load): ";
    for (int y = 0; y < imageSize; y++)
    {
        for (int x = 0; x < imageSize; x++)
        {
            Ray pixelRay = cam.getRayForPixel(x, y, imageSize);
            Color pixelColor = raytrace(pixelRay);
            pixels.push_back((unsigned char) (pixelColor.getRed() * 255));
            pixels.push_back((unsigned char) (pixelColor.getGreen() * 255));
            pixels.push_back((unsigned char) (pixelColor.getBlue() * 255));

        }
    }

    GLWindow displayWindow(imageSize, imageSize);
    displayWindow.init(0, NULL);
    displayWindow.loadPixelsToTexture((GLvoid *) &pixels[0]);
    cout << double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;

    if (animation == 1) {
        int running = GL_TRUE;
        while (running) {
            displayWindow.redraw();
            running = displayWindow.checkRun();
            pixels.clear();
            cam.move(Vector(0, 0, -0.01));
            for (int y = 0; y < imageSize; y++)
            {
                for (int x = 0; x < imageSize; x++)
                {
                    Ray pixelRay = cam.getRayForPixel(x, y, imageSize);
                    Color pixelColor = raytrace(pixelRay);
                    pixels.push_back((unsigned char) (pixelColor.getRed() * 255));
                    pixels.push_back((unsigned char) (pixelColor.getGreen() * 255));
                    pixels.push_back((unsigned char) (pixelColor.getBlue() * 255));
                }
            }
            displayWindow.updateTexture((GLvoid *) &pixels[0]);
            #ifdef DEBUG
            cerr << "frame finished" << endl;
            #endif
        }
        glfwTerminate();
    }
    else {
        displayWindow.loop();
    }

}

void Scene::renderToFile(const Camera &cam, int imageSize, ostream &os) const {
    os << "P3 " << imageSize << " " << imageSize << " " << 255 << endl;
    for (int y = 0; y < imageSize; y++)
    {
        for (int x = 0; x < imageSize; x++)
        {
            Ray pixelRay = cam.getRayForPixel(x, y, imageSize);
            Color pixelColor = raytrace(pixelRay);
            os << (int) (pixelColor.getRed() * 255) << " ";
            os << (int) (pixelColor.getGreen() * 255) << " ";
            os << (int) (pixelColor.getBlue() * 255) << " ";
            os << endl;
        }
    }
}

void Scene::gpuRender(const Camera &cam, int imageSize, int animation) {
    GLWindow displayWindow(imageSize, imageSize);
    displayWindow.init(0, NULL);

    initCUDA();

    GLuint texID;
    // Create a texture reference in OpenGL
    glGenTextures(1, &texID);

    dim3 textureDim(imageSize, imageSize);

    glBindTexture(GL_TEXTURE_2D, texID);
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_BORDER);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, textureDim.x, textureDim.y, 0, GL_RGBA, GL_FLOAT, NULL);
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    
   // Create a CUDA array
    cudaArray *cuda_image_array;
    cudaGraphicsResource *cuda_image_resource;
    cudaStream_t cuda_stream;

    // Bind the cuda image resource to the texture reference
    cudaGraphicsGLRegisterImage(&cuda_image_resource, texID, GL_TEXTURE_2D,
        cudaGraphicsRegisterFlagsSurfaceLoadStore);
    
    // Bind the image resource
    cudaStreamCreate(&cuda_stream);
    cudaGraphicsMapResources(1, &cuda_image_resource, cuda_stream);
    // Get an array reference for the image resource
    cudaGraphicsSubResourceGetMappedArray(&cuda_image_array, cuda_image_resource, 0, 0);

    // Bind the surface to which we'll write to the image array we just declared
    cudaBindSurfaceToArray(outputSurface, cuda_image_array);

    Scene *s = 0;
    s = copyToGPU();
    Camera *c;
    cudaMalloc((void **) &c, sizeof(cam));
    cudaMemcpy(c, &cam, sizeof(cam), cudaMemcpyHostToDevice);
    
    int t = 0;
    #ifdef DEBUG
    cerr << cudaGetErrorString(cudaGetLastError()) << endl;
    #endif

    clock_t startTime = clock();
    cout << "Render time for one frame: ";

    launchRenderKernel(textureDim, c, s, t);

    cout << double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
    t++;
    #ifdef DEBUG
    cerr << cudaGetErrorString(cudaGetLastError()) << endl;
    #endif
    displayWindow.setTexture(texID);

    if (animation == 1) {
        int running = GL_TRUE;
        while (running) {
            displayWindow.redraw();
            running = displayWindow.checkRun();
            launchRenderKernel(textureDim, c, s, (float) t / 60.0f);
            t = (t + 1) % 60;
        }
        glfwTerminate();
    }
    else {
        displayWindow.loop();
    }
    // Cleanup CUDA resources
    // TODO: cleanup
    cudaGraphicsUnmapResources(1, &cuda_image_resource, cuda_stream);
    cudaGraphicsUnregisterResource(cuda_image_resource);
    GPUcleanup(s);
    cudaFree(c);
    glDeleteTextures(1, &texID);

    cudaDeviceReset();
}


void launchRenderKernel(dim3 textureDim, Camera *c, Scene *s, float t) {
    // Launch kernel
    // set stack size
   // cudaThreadSetLimit(cudaLimitStackSize, sizeof(Vector) * 20);
    cudaDeviceSynchronize();
    dim3 block_dim(8, 8);
    dim3 grid_dim(textureDim.x/block_dim.x, textureDim.y/block_dim.y);

    moveCameraKernel<<<1, 1>>>(c, t);
    cudaDeviceSynchronize();
    renderKernel<<< grid_dim, block_dim >>>(textureDim, c, s);
    // sync threads
    cudaDeviceSynchronize();

}

void initCUDA()
{
    int deviceCount;
    cudaGetDeviceCount(&deviceCount);

    cerr << "CUDA device count: " << deviceCount << endl;
    int device = 0; //SELECT GPU HERE
    cerr << "Selecting GPU: " << device << endl;
    cudaSetDevice(device);
}


__global__ void moveCameraKernel(Camera *cam, float t) {
    cam->move(Vector(0, 0, -0.01 ));
}

__global__ void renderKernel(dim3 texture_dim, Camera *cam, Scene *s)
{
    // calculate normalized texture coordinates
    int x = blockIdx.x*blockDim.x + threadIdx.x;
    int y = blockIdx.y*blockDim.y + threadIdx.y;

    Color pixelColor = s->raytrace(cam->getRayForPixel(x, y, texture_dim.x));

    float4 data = {pixelColor.getRed(), pixelColor.getGreen(), pixelColor.getBlue(), 1.0f};
    surf2Dwrite<float4>(data,
            outputSurface, x * sizeof(float4), y, cudaBoundaryModeZero);
}



void Scene::init() {
    nObjects = objects.size();
    objectPointer = &objects[0];

    nLights = lights.size();
    lightPointer = &lights[0];
}

HEMI_DEV_CALLABLE_MEMBER
const Color Scene::getBackgroundColor(const Ray &r) const {

    //return Color(sin(r.direction.x), sin(r.direction.y), sin(r.direction.z));
    return 0.4 * sin(3.14f * (r.direction - Vector(0, 0, -1.0)).norm()) * Color(0.6, 0.6, 0.8);
}


