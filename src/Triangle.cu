#include "Triangle.h"

// formulas taken from
// http://www.cs.washington.edu/education/courses/cse457/07sp/lectures/triangle_intersection.pdf
HEMI_DEV_CALLABLE_MEMBER
float Triangle::intersection(const Ray &r) const {
	float dist = dot(N, A);
	float t = dist - dot(N, r.origin);
	float nd = dot(N, r.direction);
	if (nd == 0)
		return -1.0f;
	t /= nd;
	Vector Q = r.getPoint(t);

	bool intersects = dot(cross(B - A, Q - A), N) >= 0;
	intersects &= dot(cross(C - B, Q - B), N) >= 0;
	intersects &= dot(cross(A - C, Q - C), N) >= 0;
/*
	bool intersects_inverse;
	intersects_inverse = dot(cross(B - A, Q - A), N) <= 0;
	intersects_inverse &= dot(cross(C - B, Q - B), N) <= 0;
	intersects_inverse &= dot(cross(A - C, Q - C), N) <= 0;
*/
	if (intersects)
		return t;
	else
		return -1;

}

HEMI_DEV_CALLABLE_MEMBER
const Vector Triangle::getNormalAtPoint(const Vector &point) const {
	return N;
}

HEMI_DEV_CALLABLE_MEMBER
const Color Triangle::getColorAtPoint(const Vector &point) const {
	return surfaceColor;
}