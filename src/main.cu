#include <iostream>
// #include "GLWindow.h"
#include "Scene.h"
#include "Input.h"
#include <iostream>

map<string, SceneObjectReader> readFuncs; 

int main(int argc, char *argv[]) {
    readFuncs["triangle"] = ReadTriangle;
    readFuncs["sphere"] = ReadSphere;
    readFuncs["plane"] = ReadPlane;

    Scene scene;
    Camera *cam;
    string inputLine;

    string type;

    while (std::cin >> type) {
        if (readFuncs.find(type) != readFuncs.end())
        {
            SceneObject *newObj;
            newObj = readFuncs[type](std::cin);
            scene.addObject(newObj);
        }
        else if (type == "light")
        {
            Light *newLight;
            newLight = ReadLight(std::cin);
            scene.addLight(newLight);
        }
        else if (type == "camera")
        {
            cam = ReadCamera(std::cin);
        }
        else
        {
            std::cout<< "An error occurred." << std::endl;
        }
    }

    scene.init();
    if (argc == 4) {
        if (atoi(argv[1]) == 0)
            scene.render(*cam, atoi(argv[3]), atoi(argv[2]));
        else if (atoi(argv[1]) == 1)
            scene.gpuRender(*cam, atoi(argv[3]), atoi(argv[2]));
        else if (atoi(argv[1]) == 2)
            scene.renderToFile(*cam, atoi(argv[3]), std::cout);
    }
    else
        std::cerr << "usage: ./bin/main [mode] [animation] [resolution] < [input file]" << std::endl;
    return 0;
}
