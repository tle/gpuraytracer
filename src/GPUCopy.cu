#include "Scene.h"

using namespace std;


__global__ void copySphereKernel(int i, Color c, Vector ctr, float rad, float ref, Scene *s ){
	s->objectPointer[i] = new Sphere(c, ctr, rad, ref);
}

__global__ void copyPlaneKernel(int i, Color c, Vector n, float d, float r, Scene *s ){
	s->objectPointer[i] = new Plane(c, n, d, r);
}

__global__ void copyTriangleKernel(int i, Color c, Vector x, Vector y, Vector z, float f, Scene *s ){
	s->objectPointer[i] = new Triangle(c, x, y, z, f);
}

__global__ void copyLightKernel(int i, Vector pos, Color col, Scene *s){
	s->lightPointer[i] = new Light(pos, col);
}

__global__ void cleanupKernel(Scene *s) {
	for (int i = 0; i < s->nObjects; i++) {
		delete s->objectPointer[i];
	}

	for (int i = 0; i < s->nLights; i++) {
		delete s->lightPointer[i];
	}
}

__global__ void checkData(Scene *s) {

	for (int i = 0; i < s->nObjects; i++) {
		Color col = s->objectPointer[i]->getColor();
		printf("Object %d, color: %f %f %f \n", i, col.getRed(), col.getGreen(), col.getBlue());
	}

	for (int j = 0; j < s->nLights; j++) {
		Color col = s->lightPointer[j]->getColor();
		printf("Light %d, color: %f %f %f \n", j, col.getRed(), col.getGreen(), col.getBlue());
	}

}


void copyObject(int i, Sphere *s, Scene *scene) {
	copySphereKernel<<< 1, 1 >>>(i, s->getColor(), s->getCenter(), s->getRadius(), s->getReflectivity(), scene);
}

void copyObject(int i, Plane *p, Scene *scene) {
	copyPlaneKernel<<< 1, 1 >>>(i, p->getColor(), p->getNormal(), p->getDistance(), p->getReflectivity(), scene);
}

void copyObject(int i, Triangle *t, Scene *scene) {
	copyTriangleKernel<<< 1, 1 >>>(i, t->getColor(), t->getA(), t->getB(), t->getC(), t->getReflectivity(), scene);
}

void copyLight(int i, Light *l, Scene *scene) {
	copyLightKernel<<< 1, 1 >>>(i, l->getPosition(), l->getColor(), scene);
}

Scene * Scene::copyToGPU() {
	cerr << "Copying data onto GPU...";
	Scene *s = 0;
	Scene *src = new Scene(backgroundColor);

	SceneObject **gpuObjectPointer;
	Light **gpuLightPointer;
	cudaMalloc((void **) &gpuObjectPointer, nObjects * sizeof(SceneObject *));
	cudaMalloc((void **) &gpuLightPointer, nLights * sizeof(Light *));

	src->nObjects = nObjects;
	src->nLights = nLights;
	src->objectPointer = gpuObjectPointer;
	src->lightPointer = gpuLightPointer;

	cudaMalloc((void **) &s, sizeof(*this));
	cudaMemcpy(s, src, sizeof(*this), cudaMemcpyHostToDevice);

	for (int i = 0; i < nObjects; i++) {
		switch (objects[i]->getType()) {
			case SceneObject::SPHERE :
				copyObject(i, (Sphere *) objects[i], s);
				break; 
			case SceneObject::PLANE :
				copyObject(i, (Plane *) objects[i], s);
				break;
			case SceneObject::TRIANGLE :
				copyObject(i, (Triangle *) objects[i], s);
				break;
		}
	}

	for (int j = 0; j < nLights; j++)
		copyLight(j, lights[j], s);


	//checkData<<<1, 1>>>(s);

	cerr << " complete." << endl;
	return s;
}

void Scene::GPUcleanup(Scene *s) {
    cleanupKernel<<<1, 1>>>(s);
    cudaFree(s);
}