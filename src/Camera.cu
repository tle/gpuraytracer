#include "Camera.h"

HEMI_DEV_CALLABLE_MEMBER
const Vector Camera::getDirection() const {
    return direction;
}

HEMI_DEV_CALLABLE_MEMBER
const Vector Camera::getLocation() const {
    return location;
}

HEMI_DEV_CALLABLE_MEMBER
const Vector Camera::getUp() const {
    return cameraUp;
}

HEMI_DEV_CALLABLE_MEMBER
const Vector Camera::getRight() const {
    return cameraRight;
}

HEMI_DEV_CALLABLE_MEMBER
float Camera::getDistance() const {
    return distance;
}

HEMI_DEV_CALLABLE_MEMBER
float Camera::getFieldOfView() const {
    return fov;
}

HEMI_DEV_CALLABLE_MEMBER
Camera::Camera(Vector position, Vector lookAt, Vector up, float f) {
    location = position;
    direction = (lookAt - location).unit();
    cameraRight = cross(direction, up).unit();
    cameraUp = cross(cameraRight, direction).unit();
    fov = f;
    distance = 0.5 / tan(DEGTORAD(fov / 2)); 
}

HEMI_DEV_CALLABLE_MEMBER
Ray Camera::getRayForPixel(int x, int y, int imgSize) const {
    Vector pixelDir = distance * direction +
                  (0.5 - (float) y / (float) (imgSize - 1)) * cameraUp +
                  ((float) x / (float) (imgSize - 1) - 0.5) * cameraRight;
                  
    Ray pixelRay(location, pixelDir, true);
    
    return pixelRay;
}