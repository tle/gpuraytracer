#include "GLWindow.h"

void GLWindow::initGL() {
    // Flags for some features
    // Shading
    glShadeModel(GL_FLAT);
    // Depth Test
    glEnable(GL_DEPTH_TEST);
    // Lighting
    glDisable(GL_LIGHTING);

    // Scene set up
    // Set up the projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, 0.1, 100.0);
    // Reset the modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void GLWindow::init(int argc, char *argv[]) {
    glfwInit();
    glfwOpenWindow(width, height, 0, 0, 0, 0, 0, 0, GLFW_WINDOW);
    initGL();
}

// Utility functions for UI
// check whether to continue looping
int GLWindow::checkRun() {
    // check if ESC pressed
    int esc_pressed = glfwGetKey( GLFW_KEY_ESC);
    // check if window is opened
    int opened = glfwGetWindowParam( GLFW_OPENED );
    return !esc_pressed && opened;
}

void GLWindow::loop() {
    int running = GL_TRUE;
    while (running) {
        redraw();
        running = checkRun();
    }
    glfwTerminate();    
}

void GLWindow::redraw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW_MATRIX);
    glLoadIdentity();

    // enable texturing
    if (textureLoaded) {
        glBindTexture(GL_TEXTURE_2D, textureID);
        glEnable(GL_TEXTURE_2D);
    }
    else
        glColor3f(1.0, 0.2, 1.0);

    // draw the primitive
    glTranslatef(0, 0, -3.0);
    glBegin(GL_QUADS);
    
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-1.0, 1.0, 0.0);

      
    glTexCoord2f(1.0, 0.0);
    glVertex3f(1.0, 1.0, 0.0);

   
    glTexCoord2f(1.0, 1.0);
    glVertex3f(1.0,-1.0, 0.0);

   
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-1.0, -1.0, 0.0);

    glEnd();

    glfwSwapBuffers();
}

void GLWindow::loadPixelsToTexture(GLvoid *pixels) {
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);       
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    textureLoaded = true;
    //glDrawPixels( width,height,GL_RGB,GL_UNSIGNED_BYTE, pixels);
}

void GLWindow::updateTexture(GLvoid *pixels) {
    glBindTexture(GL_TEXTURE_2D, textureID);       
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
}

void GLWindow::setTexture(GLuint texID) {
    textureID = texID;
    glBindTexture(GL_TEXTURE_2D, textureID);       
    textureLoaded = true;
}