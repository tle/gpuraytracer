#ifndef LIGHT_H
#define LIGHT_H

#include "Vector.h"
#include "Color.h"
#include "hemi/hemi.h"
//! A class for describing lights in the raytraced scene.
class HEMI_ALIGN(16) Light {
    private:
        Vector position;
        Color color;
    public:
        //! Default constructor
        /*!
        * Initializes a black light at (0, 0, 0)
        */
        HEMI_DEV_CALLABLE_MEMBER
        Light() : position(Vector()), color(Color()) { };
        //! Constructor for specifying light position and color.
        HEMI_DEV_CALLABLE_MEMBER
        Light(Vector pos, Color col) : position(pos), color(col) { };
        //! Returns the position of the light.
        HEMI_DEV_CALLABLE_MEMBER
        const Vector getPosition() const{
            return position;
        }
        //! Returns the color of the light.
        HEMI_DEV_CALLABLE_MEMBER
        const Color getColor() const {
            return color;
        }
};
#endif