#ifndef PLANE_H
#define PLANE_H

#include "SceneObject.h"
#include "hemi/hemi.h"

class HEMI_ALIGN(16) Plane: public SceneObject {
    private:
        Vector surfaceNormal;
        float distance;
    public:
    	HEMI_DEV_CALLABLE_MEMBER
        Plane(const Color &c, const Vector &n, float d, float r) :
            SceneObject(c, r, PLANE), surfaceNormal(n.unit()), distance(d) {  };
        HEMI_DEV_CALLABLE_MEMBER   
        float intersection(const Ray &r) const;
        HEMI_DEV_CALLABLE_MEMBER
        const Vector getNormalAtPoint(const Vector &point) const;
        HEMI_DEV_CALLABLE_MEMBER
        const Color getColorAtPoint(const Vector &point) const;

        HEMI_DEV_CALLABLE_MEMBER
        const Vector getNormal() const { return surfaceNormal; };
        HEMI_DEV_CALLABLE_MEMBER
        float getDistance() const { return distance; };
        
};

#endif