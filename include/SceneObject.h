#ifndef SCENE_OBJECT_H
#define SCENE_OBJECT_H

#include "Color.h"
#include "Vector.h"
#include "Ray.h"
#define NO_INTERSECTION -1

#include "hemi/hemi.h"


//! General class for Scene objects
class HEMI_ALIGN(16) SceneObject {
    protected:
        Color surfaceColor;
        float reflectivity;
        int type;
    public:
        enum ObjectType {
            NONE = 0, TRIANGLE = 1, SPHERE = 2, PLANE = 3
        };
        HEMI_DEV_CALLABLE_MEMBER
        SceneObject() : surfaceColor(Color(0.5, 0.5, 0.5)), reflectivity(0), type(NONE) { };

        HEMI_DEV_CALLABLE_MEMBER
        SceneObject(const Color &c, float r, int oType) :
            surfaceColor(c), reflectivity(r), type(oType) { };

        HEMI_DEV_CALLABLE_MEMBER
        void setColor(const Color &c)  {
            surfaceColor = c;
        };

        HEMI_DEV_CALLABLE_MEMBER
        const Color getColor() const {
            return surfaceColor;
        };
        
        HEMI_DEV_CALLABLE_MEMBER
        float getReflectivity() const {
            return reflectivity;
        };
        
        HEMI_DEV_CALLABLE_MEMBER
        virtual float intersection(const Ray &r) const = 0;

        HEMI_DEV_CALLABLE_MEMBER
        virtual const Vector getNormalAtPoint(const Vector &point) const = 0;

        HEMI_DEV_CALLABLE_MEMBER
        virtual const Color getColorAtPoint(const Vector &point) const = 0;

        HEMI_DEV_CALLABLE_MEMBER
        int getType() const {return type;};
};
#endif