#ifndef GLWINDOW_H
#define GLWINDOW_H
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glfw.h>

#include <iostream>
//! A GLWindow class for displaying CUDA output.
/*!
*   Currently unused.
*/

typedef unsigned char u8;

class GLWindow {
    private:
        int width;
        int height;
        GLuint textureID;
        bool textureLoaded;
        void initGL();
    public:
        //! Class constructor
        /*!
        * Initializes a GLWindow of given width and height.
        */
        GLWindow(int w, int h) : width(w), height(h), textureLoaded(false) { }
        //! Runs OpenGL initialization routines.
        void init(int argc, char *argv[]);
        //! Starts the OpenGL window loop.
        void loop();

        //! Loads an array of pixels into a texture
        void loadPixelsToTexture(GLvoid *pixels);
        
        int checkRun();
        virtual void redraw();
        //! Sets the displayed texture to given texture ID.
        void setTexture(GLuint texID);
        //! Updates the texture
        void updateTexture(GLvoid *pixels);

};       
#endif