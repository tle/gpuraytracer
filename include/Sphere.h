#ifndef SPHERE_H
#define SPHERE_H
#include "SceneObject.h"
#include "hemi/hemi.h"

class HEMI_ALIGN(16) Sphere: public SceneObject {
    private:
        Vector center;
        float radius;
    public:
        HEMI_DEV_CALLABLE_MEMBER
        Sphere(const Color &c, const Vector &cen, float rad, float ref) :
            SceneObject(c, ref, SPHERE), center(cen), radius(rad) { };
        HEMI_DEV_CALLABLE_MEMBER
        int getIntersections(const Ray &r, float &t1, float &t2) const;
        HEMI_DEV_CALLABLE_MEMBER
        float intersection(const Ray &r) const;
        HEMI_DEV_CALLABLE_MEMBER
        const Vector getNormalAtPoint(const Vector &point) const;
        HEMI_DEV_CALLABLE_MEMBER
        const Color getColorAtPoint(const Vector &point) const;

        HEMI_DEV_CALLABLE_MEMBER
        float getRadius() const { return radius;};
        HEMI_DEV_CALLABLE_MEMBER
        const Vector getCenter() const {return center;};

};

#endif