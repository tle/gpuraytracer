#ifndef COLOR_H
#define COLOR_H

#include <stdexcept>
#include <ostream>
#include <limits>
#include <cmath>

#include "hemi/hemi.h"


//! Color class used for defining colors in RGB color space.
/*!
    The color class supports arithmetic operations. Allowed operations can
    involve colors and scalars.
    The color class clamps its fields between 0 and 1 - should any operation
    exceed that range, the Color class will clamp the values.
*/
class HEMI_ALIGN(16) Color {
    private:
        float r, g, b;
        HEMI_DEV_CALLABLE_MEMBER
        bool valid(float value) const;
        HEMI_DEV_CALLABLE_MEMBER
        float clip(float value) const;        
    public:
        //! Default constructor, initializes the color to (0, 0, 0) - black.
        HEMI_DEV_CALLABLE_MEMBER
        Color() : r(0), g(0), b(0) { } ;
        //! Constructor for specifying the RGB components between 0 and 1.
        HEMI_DEV_CALLABLE_MEMBER
        Color(float red, float green, float blue);
        //! Copy constructor.
        HEMI_DEV_CALLABLE_MEMBER
        Color(const Color &c);
        
        //! Accessor for the Red component of the color.
        HEMI_DEV_CALLABLE_MEMBER
        float getRed() const;
        //! Accessor for the Green component of the color.
        HEMI_DEV_CALLABLE_MEMBER
        float getGreen() const;
        //! Accessor for the Blue component of the color.
        HEMI_DEV_CALLABLE_MEMBER
        float getBlue() const;
        
        //! Mutator for the red component of the color.
        /*!
        * Throws std::invalid_argument if the value is not between 0 and 1
        */
        HEMI_DEV_CALLABLE_MEMBER
        void setRed(float value);
        //! Mutator for the green component of the color.
        /*!
        * Throws std::invalid_argument if the value is not between 0 and 1
        */
        HEMI_DEV_CALLABLE_MEMBER
        void setGreen(float value);
        //! Mutator for the blue component of the color
        /*!
        * Throws std::invalid_argument if the value is not between 0 and 1
        */
        HEMI_DEV_CALLABLE_MEMBER
        void setBlue(float value);
        
        HEMI_DEV_CALLABLE_MEMBER
        bool operator==(const Color &c) const;
        
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator=(const Color &c);
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator+=(const Color &c);
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator*=(const Color &c);
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator-=(const Color &c);
        
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator+=(float s);
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator-=(float s);
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator*=(float s);
        HEMI_DEV_CALLABLE_MEMBER
        Color & operator/=(float s);
        
        HEMI_DEV_CALLABLE_MEMBER
        const Color operator+(const Color &c) const;
        HEMI_DEV_CALLABLE_MEMBER
        const Color operator-(const Color &c) const;
        HEMI_DEV_CALLABLE_MEMBER
        const Color operator*(const Color &c) const;
        };

HEMI_DEV_CALLABLE
const Color operator+(const Color &c, float s);
HEMI_DEV_CALLABLE
const Color operator+(float s, const Color &c);
HEMI_DEV_CALLABLE
const Color operator*(const Color &c, float s);
HEMI_DEV_CALLABLE
const Color operator*(float s, const Color &c);
HEMI_DEV_CALLABLE
const Color operator/(const Color &c, float s);

std::ostream & operator<<(std::ostream &os, const Color &c);
#endif