#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "SceneObject.h"
#include "Vector.h"

#include "hemi/hemi.h"

class HEMI_ALIGN(16) Triangle: public SceneObject {
    private:
        // the three vertices of the triangle
        Vector A;
        Vector B;
        Vector C;
        // normal vector
        Vector N;
    public:
        HEMI_DEV_CALLABLE_MEMBER
        Triangle(const Color &c, const Vector &p0, const Vector &p1, const Vector &p2,
                float ref) : 
            SceneObject(c, ref, TRIANGLE), A(p0), B(p1), C(p2) {
                N = cross(B - A, C - A).unit();
            };

        HEMI_DEV_CALLABLE_MEMBER
        float intersection(const Ray &r) const;

        HEMI_DEV_CALLABLE_MEMBER
        const Vector getNormalAtPoint(const Vector &point) const;

        HEMI_DEV_CALLABLE_MEMBER
        const Color getColorAtPoint(const Vector &point) const;

        HEMI_DEV_CALLABLE_MEMBER
        const Vector getA() { return A; };

        HEMI_DEV_CALLABLE_MEMBER
        const Vector getB() { return B; };

        HEMI_DEV_CALLABLE_MEMBER
        const Vector getC() { return C; };
};

#endif