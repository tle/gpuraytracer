#ifndef SCENE_H
#define SCENE_H

#include "SceneObject.h"
#include "Sphere.h"
#include "Plane.h"
#include "Triangle.h"

#include "Light.h"
#include "Camera.h"
#include "GLWindow.h"

#include <stdexcept>
#include <algorithm>
#include <ostream>
#include <vector>
#include <limits>
#include <math.h>

#include "cuda.h"
#include "cuda_runtime_api.h"
#include "cuda_gl_interop.h"
#include "math_functions.h"
#include <vector_types.h>
#include "hemi/hemi.h"

#include <ctime>

#define MAX_DEPTH 4
#define INF 32767.0f

using namespace std;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef std::vector<SceneObject *>::const_iterator objectIter;
typedef std::vector<Light *>::const_iterator lightIter;

struct DeleteObject { 
    template<typename T> 
    void operator()(const T *ptr) const { 
        delete ptr; 
    } 
};
#endif

//! Scene rendered by the raytracer
class HEMI_ALIGN(16) Scene {
    private:
        const Color backgroundColor;
    public:
        vector<SceneObject *> objects;
        vector<Light *> lights;
        int nObjects;
        SceneObject **objectPointer;
        int nLights;
        Light **lightPointer;

        //! Creates an empty, black scene.
        Scene() : backgroundColor(Color(0, 0, 0)) { };
        //! Creates a scene with given Color.
        Scene(const Color &col) : backgroundColor(Color(col)) { };
        //! Creates a scene with background color of given coordinates
        Scene(float r, float g, float b) : backgroundColor(Color(r, g, b)) { };
        ~Scene();
        //! Adds a SceneObject to the scene.
        void addObject(SceneObject *obj);
        //! Adds a light to the scene
        void addLight(Light *lt);
        

        //! Initializes the raytracer for operation.
        void init();

        //! Helper function for copying data onto the GPU.
        Scene * copyToGPU();
        void GPUcleanup(Scene *s);

        //! Finds the closest object in the scene that intersects given ray.
        /*!
        *   Finds the closest object in the scene by iterating through objects
        *   in the scene.
        */
        HEMI_DEV_CALLABLE_MEMBER
        SceneObject * findClosestObject(const Ray &r, float &tIntersect) const;
        
        //! Finds the color of the ray in the scene.
        HEMI_DEV_CALLABLE_MEMBER
        const Color raytrace(const Ray &r, int depth = 0) const;

        //! Renders the scene.
        /*!
        *   Using the specified Camera object, the function computes the NDC
        *   pixel coordinates and shoots rays through the scene to render
        *   the image to given output stream.
        */
        void render(const Camera &cam, int imageSize, int animation) const;
        void renderToFile(const Camera &cam, int imageSize, ostream &os) const;
        void gpuRender(const Camera &cam, int imageSize, int animation);

        //! Returns background color for the given ray.
        HEMI_DEV_CALLABLE_MEMBER
        const Color getBackgroundColor(const Ray &r) const;

};

//! Cuda definitions
__global__ void renderKernel(dim3 textureDim, Camera *cam, Scene *s);
extern surface<void, 2> outputSurface;
void initCUDA();
__global__ void moveCameraKernel(Camera *cam, float t);
void launchRenderKernel(dim3 textureDim, Camera *c, Scene *s, float t);


// In GPUCopy.cu
__global__ void copySphereKernel(int i, Color c, Vector ctr, float rad, float ref, Scene *s );
__global__ void copyPlaneKernel(int i, Color c, Vector n, float d, float r, Scene *s );
__global__ void copyTriangleKernel(int i, Color c, Vector x, Vector y, Vector z, Scene *s );
__global__ void copyLightKernel(int i, Vector pos, Color col, Scene *s);
__global__ void cleanupKernel(Scene *s);
void copyObject(int i, Sphere *s, Scene *scene);
void copyObject(int i, Plane *p, Scene *scene);
void copyObject(int i, Triangle *t, Scene *scene);

#endif