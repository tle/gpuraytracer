#ifndef VECTOR_H
#define VECTOR_H

#include <cassert>
#include <cmath>
#include <iostream>
#include <stdexcept>

#include "hemi/hemi.h"

class HEMI_ALIGN(16) Vector {
    public:
        //! Vector x coordinate.
        float x;
        //! Vector y coordinate.
        float y;
        //! Vector z coordinate.
        float z;
        //! Default constructor initializes a (0, 0, 0) vector.
        HEMI_DEV_CALLABLE_MEMBER
        Vector() : x(0), y(0), z(0) { }
        HEMI_DEV_CALLABLE_MEMBER
        Vector(float a, float b, float c) : x(a), y(b), z(c) { }
        //! Copy constructor.
        HEMI_DEV_CALLABLE_MEMBER
        Vector(const Vector &v) : x(v.x), y(v.y), z(v.z) { }
        
        //! Equality operator.
        HEMI_DEV_CALLABLE_MEMBER
        bool operator==(const Vector &v) const;
        HEMI_DEV_CALLABLE_MEMBER
        bool operator!=(const Vector &v) const;
        
        // Self assignment operations on Scalars
        // addition
        HEMI_DEV_CALLABLE_MEMBER
        Vector & operator+=(float number);
        // subtraction
        HEMI_DEV_CALLABLE_MEMBER
        Vector & operator-=(float number);
        // division
        HEMI_DEV_CALLABLE_MEMBER
        Vector & operator/=(float number);
        // multiplication
        HEMI_DEV_CALLABLE_MEMBER
        Vector & operator*=(float number);
        
        // Self assignment operations on Vectors
        HEMI_DEV_CALLABLE_MEMBER
        Vector & operator=(const Vector &v);
        HEMI_DEV_CALLABLE_MEMBER
        Vector & operator+=(const Vector &v);
        HEMI_DEV_CALLABLE_MEMBER
        Vector & operator-=(const Vector &v);
        
        //! Returns the length of the vector.
        HEMI_DEV_CALLABLE_MEMBER
        float norm() const;
        //! Returns unit vector in the direction of the vector.
        /*!
        *   Throws <b>std::length_error</b> if the length of the vector is 0.
        */
        HEMI_DEV_CALLABLE_MEMBER
        Vector unit() const;
        //! Returns whether given vector is a unit vector
        /*!
        *   Throws <b>std::length_error</b> if the length of the vector is 0.
        */
        HEMI_DEV_CALLABLE_MEMBER
        bool isUnit() const;
        
};

// Vector-Scalar and Scalar-Vector operations
// Scalar addition
HEMI_DEV_CALLABLE
const Vector operator+(const Vector &v, float s);
HEMI_DEV_CALLABLE
const Vector operator+(float s, const Vector &v);
// Scalar subtraction
HEMI_DEV_CALLABLE
const Vector operator-(const Vector &v, float s);
HEMI_DEV_CALLABLE
const Vector operator-(float s, const Vector &v);
// Scalar multiplication
HEMI_DEV_CALLABLE
const Vector operator*(const Vector &v, float s);
HEMI_DEV_CALLABLE
const Vector operator*(float s, const Vector &v);
// Scalar division (only right)
HEMI_DEV_CALLABLE
const Vector operator/(const Vector &v, float s);


// Vector - Vector operations
// Addition
HEMI_DEV_CALLABLE
const Vector operator+(const Vector &v, const Vector &s);
// Subtraction
HEMI_DEV_CALLABLE
const Vector operator-(const Vector &v, const Vector &s);
// Dot product
HEMI_DEV_CALLABLE
float dot(const Vector &u, const Vector &v);
// Cross product
HEMI_DEV_CALLABLE
const Vector cross(const Vector &u, const Vector &v);
// Vector projection
HEMI_DEV_CALLABLE
const Vector project(const Vector &u, const Vector &v);

// Vector negation
HEMI_DEV_CALLABLE
const Vector operator-(const Vector &v);

// Printing
std::ostream& operator<<(std::ostream& os, const Vector &v);
#endif
