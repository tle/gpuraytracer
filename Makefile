CXX 		= g++ -g
NVCC		= nvcc
CXXFLAGS	= 

SM_ARCH 	= sm_21
CUFLAGS		= -arch=$(SM_ARCH) -lcuda -lcudart
CULIBS		= -L/usr/local/cuda-5.0/lib64

OPENGL		= -lGL -lGLU -lglfw 
# Linux
LIBS		= $(CULIBS) $(OPENGL)
INCLUDES	= -I./include

CFLAGS		= -G -g $(CXXFLAGS) $(INCLUDES) $(CUFLAGS)
CPP_CFLAGS	= $(CXXFLAGS) $(INCLUDES)
LFLAGS		= -G -g -arch=$(SM_ARCH)

TARGET		= bin/main

OBJS		= objs/Scene.o objs/Vector.o objs/Color.o objs/Ray.o \
				 objs/Camera.o objs/Input.o \
				objs/Triangle.o objs/GLWindow.o \
				objs/Sphere.o objs/Plane.o \
				objs/main.o objs/GPUCopy.o

MKDIR_P		= mkdir -p
.PHONY		= directories
OUT_DIR		= bin objs
directories	= $(OUT_DIR)

all: directories $(TARGET)

directories: $(OUT_DIR)

$(OUT_DIR):
	$(MKDIR_P) $(OUT_DIR)

$(TARGET): $(OBJS)
	$(NVCC) $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

objs/main.o: src/main.cu
	$(NVCC) $(CFLAGS) -dc src/main.cu -o objs/main.o

objs/Vector.o: src/Vector.cu
	$(NVCC) $(CFLAGS) -dc src/Vector.cu -o objs/Vector.o

objs/Color.o: src/Color.cu
	$(NVCC) $(CFLAGS) -dc src/Color.cu -o objs/Color.o
			
objs/Ray.o: src/Ray.cu
	$(NVCC) $(CFLAGS) -dc src/Ray.cu -o objs/Ray.o

objs/Scene.o: src/Scene.cu
	$(NVCC) $(CFLAGS) -dc src/Scene.cu -o objs/Scene.o

objs/Camera.o: src/Camera.cu
	$(NVCC) $(CFLAGS) -dc src/Camera.cu -o objs/Camera.o

objs/Input.o: src/Input.cpp
	$(CXX) $(CPP_CFLAGS) -c src/Input.cpp -o objs/Input.o

objs/GLWindow.o: src/GLWindow.cpp
	$(CXX) $(CPP_CFLAGS) -c src/GLWindow.cpp -o objs/GLWindow.o

objs/Triangle.o: src/Triangle.cu
	$(NVCC) $(CFLAGS) -dc src/Triangle.cu -o objs/Triangle.o

objs/Sphere.o: src/Sphere.cu
	$(NVCC) $(CFLAGS) -dc src/Sphere.cu -o objs/Sphere.o

objs/Plane.o: src/Plane.cu
	$(NVCC) $(CFLAGS) -dc src/Plane.cu -o objs/Plane.o

objs/GPUCopy.o: src/GPUCopy.cu
	$(NVCC) $(CFLAGS) -dc src/GPUCopy.cu -o objs/GPUCopy.o

TEST_CFLAGS = $(INCLUDES)
TEST_CFLAGS += -I test/include
TEST_LFLAGS = -lcudart -lcuda 
GTEST_OBJS = test/objs/gtest-all.o
TEST_OBJS = test/objs/test.o test/objs/VectorTest.o

TESTED_OBJS = objs/Vector.o

ALL_TEST_OBJS = $(TESTED_OBJS) $(TEST_OBJS) $(GTEST_OBJS)

TEST_TARGET = bin/test

test: $(TEST_TARGET)
	
bin/test: $(ALL_TEST_OBJS)
		$(CXX) $(TEST_LFLAGS) -pthread $(ALL_TEST_OBJS) -o $(TEST_TARGET)

test/objs/test.o: test/RunTests.cpp
		$(CXX) $(TEST_CFLAGS) -c test/RunTests.cpp -o test/objs/test.o

test/objs/VectorTest.o: test/VectorTest.cpp
		$(CXX) $(TEST_CFLAGS) -c test/VectorTest.cpp -o test/objs/VectorTest.o

clean:
	rm -f $(OBJS)
	rm -f $(TARGET)

cleantest:
	rm -f $(TEST_TARGET)
	rm -f $(TEST_OBJS)
